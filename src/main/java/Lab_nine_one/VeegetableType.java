/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Lab_nine_one;

/**
 *
 * @author Kunj Prajapari
 */
public enum VeegetableType
{
    BROCCOLI,
    SPINACH,
    TOMATO,
    CARROT,
    BEET
}
