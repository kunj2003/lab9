/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package LabNine_two;

/**
 *
 * @author Kunj Prajapati
 * 
 */
public class Associate extends Employee{
    public Associate(String name, double wages, double hours) {
        super(name, wages, hours);
    }
    
    //getters
    @Override
    public String getName() {return super.name;}
    public double getWage() {return super.wage;}
    public double getHours() {return super.hours;}
    
    
    
    //method(s)
    @Override
    public double totalWage(double wage, double hours) {
       return wage*hours;
    }  
}
