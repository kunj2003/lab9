/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package LabNine_two;

/**
 *
 * @author Kunj Prajapati
 */
public abstract class Employee {
    //attribute(s)
    protected String name;
    protected double wage;
    protected double hours;
    
    
    
    //constructor(s)
        public Employee(String name, double wage, double hours) {
        this.name = name;
        this.wage = wage;
        this.hours =hours;
        
    } 
    
    //getter(s)
    public abstract String getName();
    public abstract double getWage();
    public abstract double getHours();
    
    
    //method(s)
    public abstract double totalWage(double wage, double hours);

}

