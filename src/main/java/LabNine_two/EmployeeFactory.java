/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package LabNine_two;

/**
 *
 * @author Kunj Prajapati
 */
public class EmployeeFactory {
    private static EmployeeFactory factory;
    
    //private default constructor --> so that  no one can instantiate from outside
    private EmployeeFactory()
    {}
    
    public static EmployeeFactory getInstance()
    {
        if(factory==null)
            factory = new EmployeeFactory();
        return factory;
    }
   
    public Employee getEmployee(EmployeeType type,String name,double wage, double hours)
    {
        switch( type )
        {
            case ASSOCIATE : return new Associate(name, wage, hours);
        }
        
        
        
        
        return null;
    }
}
