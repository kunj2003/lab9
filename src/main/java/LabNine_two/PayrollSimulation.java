/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package LabNine_two;



/**
 *
 * @author Kunj Prajapati
 */
public class PayrollSimulation {
    
    EmployeeFactory factory = EmployeeFactory.getInstance();
    
    Employee associate1= factory.getEmployee(EmployeeType.ASSOCIATE, "KUNJ", 15.25,20.00);
    Employee manager1= factory.getEmployee(EmployeeType.MANAGER, "JOHN", 25.25,40.00);
    
    System.out.println("Name of employee is  " + associate1.getName() +" His salary is " + associate1.totalWage);
    System.out.println("Name of employee is  " + manager1.getName() +" His salary is " + manager1.totalWage);
    
    
 
}
